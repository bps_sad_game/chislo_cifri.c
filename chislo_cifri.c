

#include <stdio.h>

int factor_digits(int num, int *digits, int limit)
{
    int digits_count = 0;


    if (num == 0) {
        digits[0] = 0;
        return 1;
    }


    while (num > 0 && limit) {

        digits[digits_count++] = num % 10;

        num /= 10;
       
        limit--;
   }
   return num ? 0 : digits_count;
}


int main()
{

    int n = 0;   
    scanf("%d",&n);     
    int factored_number[10]; 
    int factored_number_len;   
    int i;
    int last = 0;
    factored_number_len = factor_digits(n, factored_number, 10);

    
    for (i = factored_number_len - 1; i >= 0; i--) {
       
     printf("%d\n",factored_number[i]);
    }
    return 0;
}

